from plyfile import PlyData, PlyElement
from Vectors import Vectors
# import matplotlib.pyplot as plt
import numpy as np
import os, sys, csv
import math, random
import pyvista
import pyacvd
from Features_extraction import Features_extraction as fe
import time
import warnings

class Mesh():

    def __init__(self, conv_mode = False):

        self.vertices = []
        self.faces = []
        self.faces_containing_vertex = []
        self.bounding_box = {
            "xmin" : 999.0,
            "xmax" : -999.0,
            "ymin" : 999.0,
            "ymax" : -999.0,
            "zmin" : 999.0,
            "zmax" : -999.0
            }
        self.metadata = {}
        self.test_vector = np.array([1,1,1], dtype = float)
        self.conv_mode = conv_mode

    def import_mesh(self, file, category):
        start_time = time.time()
        print("file: " + file)
        if file.split('.')[len(file.split('.'))-1] == "off":
            #converter from off to ply
            ply_file = self.off2ply(file)
            self.upsampling(ply_file.name, 40000)
        elif file.split('.')[len(file.split('.'))-1] == "ply":
            
            self.import_from_ply(file)
        end_time = time.time()
        print('import')
        if not self.conv_mode:
            print(start_time - end_time)
            start_time = time.time()
            #self.triangulate()
            self.metadata['filename'] = file
            end_time = time.time()
            print('filename')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['category'] = category
            end_time = time.time()
            print('category')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['num_of_vertices'] = self.num_of_vertices()
            end_time = time.time()
            print('num_of_vertices')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['num_of_faces'] = self.num_of_faces()
            end_time = time.time()
            print('num_of_faces')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['type_of_faces'] = self.type_of_faces()
            end_time = time.time()
            print('type_of_faces')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['surface_area'] = self.surface_area()
            end_time = time.time()
            print('surface_area')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['volume'] = self.volume()
            end_time = time.time()
            print('volume')
            print(start_time - end_time)
            
            start_time = time.time()
            self.normalize_position()
            end_time = time.time()
            print('normalize_position')
            print(start_time - end_time)
            start_time = time.time()
            self.normalize_shape()
            end_time = time.time()
            print('normalize_shape')
            print(start_time - end_time)
            start_time = time.time()

            self.metadata['principal_components'] = self.principal_components()
            end_time = time.time()
            print('principal_components')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['eccentricity'] = self.eccentricity()
            end_time = time.time()
            print('eccentricity')
            print(start_time - end_time)
            start_time = time.time()

            self.normalize_rotation()
            end_time = time.time()
            print('normalize_rotation')
            print(start_time - end_time)
            start_time = time.time()
            self.normalize_flip()
            end_time = time.time()
            print('normalize_flip')
            print(start_time - end_time)
            start_time = time.time()

            self.bounding_box = self.calculate_bounding_box()
            end_time = time.time()
            print('calculate_bounding_box')
            print(start_time - end_time)
            start_time = time.time()
            # self.normalize_shape()
            # end_time = time.time()
            # print('normalize_shape')
            # print(start_time - end_time)
            # start_time = time.time()

            # self.bounding_box = self.calculate_bounding_box()
            # end_time = time.time()
            # print('calculate_bounding_box')
            # print(start_time - end_time)
            # start_time = time.time()
            # self.metadata['barycenter'] = self.barycenter()
            # end_time = time.time()
            # print('barycenter')
            # print(start_time - end_time)
            

            # self.metadata['bounding_box_dimensions'] = (self.bounding_box["xmax"] - self.bounding_box["xmin"], self.bounding_box["ymax"] - self.bounding_box["ymin"], self.bounding_box["ymax"] - self.bounding_box["ymin"])
            # self.metadata['bounding_box_volume'] = self.metadata['bounding_box_dimensions'][0] * self.metadata['bounding_box_dimensions'][1] * self.metadata['bounding_box_dimensions'][2]
            start_time = time.time()
            self.metadata['average_face_area'] = self.average_face_area()
            end_time = time.time()
            print('average_face_area')
            print(start_time - end_time)
            start_time = time.time()
            self.metadata['circularity'] = self.circularity()
            end_time = time.time()
            print('circularity')
            print(start_time - end_time)

            start_time = time.time()
            histograms = self.calculate_histograms()
            end_time = time.time()
            print('histograms')
            print(start_time - end_time)
            self.metadata['angles_3_vertices'] = histograms['angles_3_vertices']
            self.metadata['distances_to_barycenter'] = histograms['distances_to_barycenter']
            self.metadata['distances_2_vertices'] = histograms['distances_2_vertices']
            self.metadata['square_roots_area_3_vertices'] = histograms['square_roots_area_3_vertices']
            self.metadata['cube_roots_tetrahedron_4_vertices'] = histograms['cube_roots_tetrahedron_4_vertices']
            

    def import_from_ply(self, file):
        pd = PlyData()
        conv_file = pd.read(file)

        for vertex in conv_file['vertex']:
            #self.vertices.append(Vertex(vertex['x'],vertex['y'],vertex['z'],vertex['nx'],vertex['ny'],vertex['nz']))
            self.vertices.append(Vertex(vertex['x'],vertex['y'],vertex['z']))

            if vertex['x'] > self.bounding_box['xmax']:
                self.bounding_box['xmax'] = vertex['x']
            if vertex['x'] < self.bounding_box['xmin']:
                self.bounding_box['xmin'] = vertex['x']
            if vertex['y'] > self.bounding_box['ymax']:
                self.bounding_box['ymax'] = vertex['y']
            if vertex['y'] < self.bounding_box['ymin']:
                self.bounding_box['ymin'] = vertex['y']
            if vertex['z'] > self.bounding_box['zmax']:
                self.bounding_box['zmax'] = vertex['z']
            if vertex['z'] < self.bounding_box['zmin']:
                self.bounding_box['zmin'] = vertex['z']
        self.vertices = np.array(self.vertices, dtype = Vertex)
        #self.faces_containing_vertex = list(np.zeros(len(self.vertices)))
        #print(self.faces_containing_vertex)
        #print(self.bounding_box)
        for face in conv_file['face']:
            f = Face(face[0], self)
            self.faces.append(f)
            # for vertex in f.vertices:
            #     if self.faces_containing_vertex[vertex] == 0:
            #         self.faces_containing_vertex[vertex] = []
            #     self.faces_containing_vertex[vertex].append(f)

        self.faces = np.array(self.faces, dtype = Face)
    
    def export_to_ply(self, file):

        outFile = open(file, "w+")
        outFile.writelines("ply" "\nformat ascii 1.0" "\ncomment Created by Paolo" "\nelement vertex " + str(len(self.vertices)) +
                "\nproperty float x" "\nproperty float y" "\nproperty float z" "\nelement face " +
                    str(len(self.faces)) + "\nproperty list uchar uint vertex_indices" "\nend_header \n")
        for e in self.vertices: #_with_normals:
            outFile.write(str(e) + "\n")
        for f in self.faces:
            outFile.write(str(f) + "\n")
        outFile.close()
        return outFile

    def off2ply(self, f):

        vertices = []
        faces = []
        #faces_containing_vertex = []
        #vertices_with_normals = []

        file = open(f)
        name_file = f.rsplit('.', 1)[0]
        lines = file.readlines()
        n_vertices = int(lines[1].split(" ")[0])
        n_faces = int(lines[1].split(" ")[1])

        for line in lines[2 : 2 + n_vertices]:
            vertices.append(line)

        for line in lines[2 + n_vertices : 2 + n_vertices + n_faces]:
            faces.append(line)

        '''
        for vertex in vertices:
            n = 0
            for face in faces[faces_containing_vertex[vertex]]:
                face_normal = np.cross(vertices[face[1]]-vertices[face[2]], vertices[face[2]] - vertices[face[3]])
                sum_face_normal += face_normal
                n += 1
            vertex_normal = sum_face_normal / n
            vertices_with_normals.append(vertex, vertex_normal)
        '''

        outFile = open(name_file + ".ply", "w+")
        outFile.writelines("ply" "\nformat ascii 1.0" "\ncomment Created by Paolo" "\nelement vertex " + str(n_vertices) +
                "\nproperty float x" "\nproperty float y" "\nproperty float z" "\nelement face " +
                    str(n_faces) + "\nproperty list uchar uint vertex_indices" "\nend_header \n")
        for e in vertices: #_with_normals:
            outFile.write(str(e))
        for f in faces:
            outFile.write(str(f))
        outFile.close()
        return outFile

    def volume(self):
        volume = 0
        for face in self.faces:
            volume += face.signed_volume()

        return volume

    def circularity(self):
        volume = self.metadata["volume"]
        area = self.metadata["surface_area"]

        return (27 * math.pi * volume * volume) / (area * area * area)

    def normalize(self):
        self.normalize_position()
        self.normalize_shape()
        self.normalize_rotation()

    def barycenter(self):
        barycenter = np.array([0.,0.,0.], dtype=float)
        for vertex in self.vertices:
            barycenter[0] += vertex.X
            barycenter[1] += vertex.Y
            barycenter[2] += vertex.Z
        barycenter = barycenter / len(self.vertices)
        return barycenter

    def normalize_position(self):
        barycenter = self.barycenter()
        for vertex in self.vertices:
            vertex.X -= barycenter[0]
            vertex.Y -= barycenter[1]
            vertex.Z -= barycenter[2]
        for val in self.bounding_box:
            if val[0] == 'x':
                self.bounding_box[val] -=barycenter[0]
            if val[0] == 'y':
                self.bounding_box[val] -=barycenter[1]
            if val[0] == 'z':
                self.bounding_box[val] -=barycenter[2]

    def normalize_shape(self):
        scale_x = self.bounding_box['xmax'] - self.bounding_box['xmin']
        scale_y = self.bounding_box['ymax'] - self.bounding_box['ymin']
        scale_z = self.bounding_box['zmax'] - self.bounding_box['zmin']
        scale = max([scale_x, scale_y, scale_z])
        for vertex in self.vertices:
            vertex.X /= scale
            vertex.Y /= scale
            vertex.Z /= scale

        for entry in self.bounding_box:
            if entry[0] == 'x':
                self.bounding_box[entry] /= scale_x
            if entry[0] == 'y':
                self.bounding_box[entry] /= scale_y
            if entry[0] == 'z':
                self.bounding_box[entry] /= scale_z

    def normalize_rotation(self):
        vector_tool = Vectors()
        principal_component_0 = np.array(self.metadata['principal_components'][0][1])
        principal_component_1 = np.array(self.metadata['principal_components'][1][1])
        principal_component_2 = np.array(self.metadata['principal_components'][2][1])
        
        
        for i, vertex in enumerate(self.vertices):
            coordinates = vertex.coords()
            self.vertices[i].X = np.dot(principal_component_0, coordinates) / vector_tool.length(principal_component_0)
            self.vertices[i].Y = np.dot(principal_component_1, coordinates) / vector_tool.length(principal_component_1)
            self.vertices[i].Z = np.dot(principal_component_2, coordinates) / vector_tool.length(principal_component_2)

        print(self.principal_components())
        


        # vector_tool = Vectors()
        # identity = np.identity(3)
        # normalized_eigenvector = self.metadata['principal_components'][0][1]
        # rotation_basis = np.cross(normalized_eigenvector, np.array([1,0,0], dtype=float))
        # skew_symmetric_cross_product = np.array([
        #     [0, -rotation_basis[2], rotation_basis[1]],
        #     [rotation_basis[2], 0, -rotation_basis[0]],
        #     [-rotation_basis[1], rotation_basis[0], 0]
        # ])
        # rotation_matrix_0 = identity + skew_symmetric_cross_product + np.dot(skew_symmetric_cross_product, skew_symmetric_cross_product) * (1 / (1 + np.dot(self.metadata['principal_components'][0][1], np.array([1,0,0], dtype=float))))

        # rotated_eigenvector_1 = rotation_matrix_0.dot(self.principal_components()[1][1])
        # angle_1 = -vector_tool.angle(rotated_eigenvector_1, np.array([0,1,0]))

        # rotation_matrix_1 = np.array([
        #     [1,0,0],
        #     [0, math.cos(angle_1), -math.sin(angle_1)],
        #     [0, math.sin(angle_1), math.cos(angle_1)]
        # ], dtype=float)

        # for i, vertex in enumerate(self.vertices):
        #     new_coords = rotation_matrix_0.dot(vertex.coords())
        #     new_coords = rotation_matrix_1.dot(new_coords)

        #     self.vertices[i].X = new_coords[0]
        #     self.vertices[i].Y = new_coords[1]
        #     self.vertices[i].Z = new_coords[2]

    def normalize_flip(self):
        x_moment = 0
        y_moment = 0
        z_moment = 0
        for vertex in self.vertices:
            if vertex.X < 0:
                x_moment += -(vertex.X * vertex.X)
            else:
                x_moment += (vertex.X * vertex.X)
            if vertex.Y < 0:
                y_moment += -(vertex.Y * vertex.Y)
            else:
                y_moment += (vertex.Y * vertex.Y)
            if vertex.Z < 0:
                z_moment += -(vertex.Z * vertex.Z)
            else:
                z_moment += (vertex.Z * vertex.Z)

        for i, vertex in enumerate(self.vertices):
            if x_moment > 0:
                self.vertices[i].X = -self.vertices[i].X
            if y_moment > 0:
                self.vertices[i].Y = -self.vertices[i].Y
            if z_moment > 0:
                self.vertices[i].Z = -self.vertices[i].Z

    def vertices_per_face(self):

        types = []
        for face in self.faces:
            if len(face) not in types:
                types.append(len(face))
        return types

    def num_of_vertices(self):
        return len(self.vertices)

    def num_of_faces(self):
        return len(self.faces)

    def type_of_faces(self):
        sizes = []
        for face in self.faces:
            if len(face) not in sizes:
                sizes.append(len(face))

        sizes.sort()
        return sizes

    def average_face_area(self):

        average_area = 0
        for face in self.faces:
            average_area += face.area()
        average_area /= len(self.faces)

        return average_area

    def principal_components(self):
        vertices = []
        for vertex in self.vertices:
            vertices.append(vertex.coords())
        transposed_vertices = np.array(vertices).T
        covariant_matrix = np.cov(transposed_vertices)

        eigenvalue, eigenvector = np.linalg.eig(covariant_matrix)
        sorted_principal_components = sorted(zip(eigenvalue, eigenvector.T), reverse=True)
        #print(sorted_principal_components)
        #return(eigenvector.T, eigenvalue)
        return sorted_principal_components

    def _calculate_std(self, axis = 'x'):
        if axis == 'x':
            data = []
            for vertex in self.vertices:
                data.append(vertex.X)
            npdata =  np.array(data, dtype = float)
            return npdata.std()

    def surface_area(self):
        surface_area = 0
        for face in self.faces:

            surface_area += face.area()
        return surface_area

    def eccentricity(self):
        high = self.metadata["principal_components"][0][0]
        low = self.metadata["principal_components"][2][0]
        return high / low

    def triangulate(self):
        faces = list(self.faces)
        for i, face in enumerate(faces):
            if len(face) == 4:
                faces[i] = face.triangulate()
        faces = np.array(faces, dtype = Face)
        self.faces = np.hstack(faces)

    def calculate_bounding_box(self):
        bounding_box = {
            "xmin" : 999.0,
            "xmax" : -999.0,
            "ymin" : 999.0,
            "ymax" : -999.0,
            "zmin" : 999.0,
            "zmax" : -999.0
            }
        for vertex in self.vertices:
            if vertex.X < bounding_box['xmin']:
                bounding_box['xmin'] = vertex.X
            if vertex.X > bounding_box['xmax']:
                bounding_box['xmax'] = vertex.X
            if vertex.Y < bounding_box['ymin']:
                bounding_box['ymin'] = vertex.Y
            if vertex.Y > bounding_box['ymax']:
                bounding_box['ymax'] = vertex.Y
            if vertex.Z < bounding_box['zmin']:
                bounding_box['zmin'] = vertex.Z
            if vertex.Z > bounding_box['zmax']:
                bounding_box['zmax'] = vertex.Z

        return bounding_box

    def print_metadata(self, plot = None):
        for entry in self.metadata:
            if entry is plot:
                print(entry)
                # frequency, bins = self.metadata[entry]
                # bins = bins[0:len(frequency)]
                # plt.bar(bins, frequency)
                # print(type(plt))
            else:
                pass
                print(entry)
                print(f"\t{self.metadata[entry]}")
        # plt.show()

    def angle_3_vertices(self, vertex_0, vertex_1, vertex_2):
        
        vector_tool = Vectors()
        vector_0 = vertex_1.coords() - vertex_0.coords()
        vector_1 = vertex_2.coords() - vertex_0.coords()

        return vector_tool.angle(vector_0, vector_1, vertex_0, vertex_1, vertex_2)

    def distance_to_barycenter(self, vertex_0):
        
        vector_tool = Vectors()
        return vector_tool.length(vertex_0.coords())

    def distance_2_vertices(self, vertex_0, vertex_1):
        vector_0 = vertex_0.coords()
        vector_1 = vertex_1.coords()
        distance = 0

        for p, q in zip(vector_0, vector_1):
            distance += (p - q)*(p - q)
        
        distance = math.sqrt(distance)
        return distance

    def square_root_area_3_vertices(self, vertex_0, vertex_1, vertex_2):
        vector_tool = Vectors()
        ab = vertex_1.coords() - vertex_0.coords()
        ac = vertex_2.coords() - vertex_0.coords()
        
        cross_product = np.cross(ab,ac)

        return math.sqrt(.5 * vector_tool.length(cross_product))

    def cube_root_tetrahedron_4_vertices(self, vertex_0, vertex_1, vertex_2, vertex_3):
        warnings.simplefilter("error")
        a = vertex_1.coords() - vertex_0.coords()
        b = vertex_2.coords() - vertex_0.coords()
        c = vertex_3.coords() - vertex_0.coords()
        try:
            cross = np.cross(a,b)
            dot = np.dot(cross, c)
            volume = abs(dot / 6)
            cube_root = volume ** 1/3
            
            
            return cube_root
        except:
            print(f"a: {a}")
            print(f"b: {b}")
            print(f"c: {c}")
            print()
            print(f"vertex_0: {vertex_0}")
            print(f"vertex_1: {vertex_1}")
            print(f"vertex_2: {vertex_2}")
            print(f"vertex_3: {vertex_3}")

    def curvature(self, vertex_0):
        vector_tool = Vectors()
        normals = []
        index = np.where(self.vertices == vertex_0)[0][0]

        for face in self.faces_containing_vertex[index]:
            normals.append(face.calculate_normal())
        print(normals)
        curvature = 0
        for normal_0 in normals:
            for normal_1 in normals:
                if not np.array_equal(normal_0, normal_1):
                    print(normal_0)
                    print(normal_1)
                    angle = vector_tool.angle(normal_0, normal_1)
                    print(angle)
                    curvature += angle
        return curvature



    def calculate_histograms(self, sample_size = 40000, bin_count = 10):
        angles_3_vertices = []
        distances_to_barycenter = []
        distances_2_vertices = []
        square_roots_area_3_vertices = []
        cube_roots_tetrahedron_4_vertices = []
        curvature = []
        print("identity?")
        x = [Vertex(0,0,0)]

        print(Vertex(0,0,0) in x)
        for i in range(0, sample_size):
            vertices = []
            while len(vertices) != 4:
                vertex = random.choice(self.vertices)
                if vertex not in vertices:
                    vertices.append(vertex)
            
            vertex_0 = vertices[0]
            vertex_1 = vertices[1]
            vertex_2 = vertices[2]
            vertex_3 = vertices[3]
            # vertex_0 = random.choice(self.vertices)
            # vertex_1 = random.choice(self.vertices)
            # vertex_2 = random.choice(self.vertices)
            # vertex_3 = random.choice(self.vertices)
            
            angles_3_vertices.append(self.angle_3_vertices(vertex_0, vertex_1, vertex_2))
            distances_to_barycenter.append(self.distance_to_barycenter(vertex_0))
            distances_2_vertices.append(self.distance_2_vertices(vertex_0, vertex_1))
            square_roots_area_3_vertices.append(self.square_root_area_3_vertices(vertex_0, vertex_1, vertex_2))
            cube_roots_tetrahedron_4_vertices.append(self.cube_root_tetrahedron_4_vertices(vertex_0, vertex_1, vertex_2, vertex_3))
            
        
        histograms = {}
        histograms['angles_3_vertices'] = np.histogram(angles_3_vertices, bins = bin_count, range = (0.0, math.pi), density = True)
        histograms['distances_to_barycenter'] = np.histogram(distances_to_barycenter, bins = bin_count, range = (0.0, 1.), density = True)
        histograms['distances_2_vertices'] = np.histogram(distances_2_vertices, bins = bin_count, range = (0.0, 1.5), density = True)
        histograms['square_roots_area_3_vertices'] = np.histogram(square_roots_area_3_vertices, bins = bin_count, range = (0.0, 1), density = True)
        histograms['cube_roots_tetrahedron_4_vertices'] = np.histogram(cube_roots_tetrahedron_4_vertices, bins = bin_count, range = (0.0, .03), density = True)
        # print(cube_roots_tetrahedron_4_vertices)
        # print(curvature)
        # histograms['curvature'] = np.histogram(cube_roots_tetrahedron_4_vertices, bins = bin_count, range = (0.0, math.pi), density = True)
        return histograms
    
    # def features_extraction(self, file, sample_size, bin_count = 10):

    #     descriptors = {}
    #     angles_list = []
    #     dist2bary_list = []
    #     dist2vertices_list = []
    #     sqrt_area_list = []
    #     cbrt_vol_tetra_list = []
    #     for vertex in self.vertices:
    #         coordinates = vertex.coords()
    #         vertexB = random.choice(self.vertices)
    #         coordinatesB = vertexB.coords()
    #         vertexC = random.choice(self.vertices)
    #         coordinatesC = vertexC.coords()
    #         vertexD = random.choice(self.vertices)
    #         coordinatesD = vertexD.coords()

    #         angles_list.append(fe.angle_3_vertices(None, coordinates, coordinatesB, coordinatesC))
    #         dist2bary_list.append(fe.dist_to_barycenter(None, coordinates))
    #         dist2vertices_list.append(fe.dist_2_vertices(None, coordinates, coordinatesB))
    #         sqrt_area_list.append(fe.sqrt_area_triangle_3_vertices(None, coordinates, coordinatesB, coordinatesC))
    #         cbrt_vol_tetra_list.append(fe.cbrt_vol_tetra_4_vertices(None, coordinates, coordinatesB, coordinatesC, coordinatesD))
    #         '''
    #         print(f"angles_list[0]: {angles_list[0]}")
    #         print(f"dist2bary_list[0]: {dist2bary_list[0]}")
    #         print(f"dist2vertices_list[0]: {dist2vertices_list[0]}")
    #         print(f"sqrt_area_list[0]: {sqrt_area_list[0]}")
    #         print(f"cbrt_vol_tetra_list[0]: {cbrt_vol_tetra_list[0]}")
    #         '''
    #         #break

    #     #print(f"angles_list: {angles_list}")
    #     #print(f"dist2bary_list[0]: {dist2bary_list[0]}")
    #     #print(f"dist2vertices_list[0]: {dist2vertices_list[0]}")
    #     #print(f"sqrt_area_list[0]: {sqrt_area_list[0]}")
    #     #print(f"cbrt_vol_tetra_list[0]: {cbrt_vol_tetra_list[0]}")

        
    #     angles_hist = np.histogram(angles_list, range = (0.0, 2 * math.pi), bins=bin_count, density=False)
    #     descriptors['angle_3_vertices'] = angles_hist
    #     dist2bary_hist = np.histogram(dist2bary_list, range = (0.0, 1.), bins=bin_count, density=True)
    #     descriptors['dist_to_barycenter'] = dist2bary_hist
    #     dist2vert_hist = np.histogram(dist2vertices_list, range = (0.0, 2 * math.pi), bins=bin_count, density=True)
    #     descriptors['dist_2_vertices'] = dist2vert_hist
    #     sqrt_area_hist = np.histogram(sqrt_area_list, range = (0.0, max(sqrt_area_list)), bins=bin_count, density=True)
    #     descriptors['sqrt_area_triangle_3_vertices'] = sqrt_area_hist
    #     cbrt_vol_hist = np.histogram(cbrt_vol_tetra_list, range = (0.0, max(sqrt_area_list)), bins=bin_count, density=True)
    #     descriptors['cbrt_vol_tetra_4_vertices'] = cbrt_vol_hist
        
    #     return descriptors

    def upsampling(self, file, sample_size):
        print(f"file: {file}")
        low_mesh = pyvista.read(file)
        
        clus = pyacvd.Clustering(low_mesh)
        clus.subdivide(3)
        clus.cluster(sample_size)
        
        upsampled_mesh = clus.create_mesh()
        upsampled_mesh.save(file)

        self.import_from_ply(file)

        


class Vertex():

    def __init__(self, x,y,z,nx=0.0,ny=0.0,nz=0.0):
        self.X = x
        self.Y = y
        self.Z = z
        self.nX = nx
        self.nY = ny
        self.nZ = nz

    def coords(self, coords = None):
        if coords is None:
            return np.array([self.X, self.Y, self.Z])
        elif type(coords) == np.ndarray:
            pass

    def has_normal(self):
        if (self.nX == 0.0 and self.nY == 0.0 and self.nZ == 0.0):
            return False
        else:
            return True

    def calculate_normal(self):
        if (self.has_normal() == False):
            pass
        else:
            print("normals are already specified")

    def __str__(self):
        return "{} {} {}".format(self.X, self.Y, self.Z)

    def __eq__(self, other):
        if type(other) == type(self):
            if self.X == other.X and self.Y == other.Y and self.Z == other.Z:
                return True

        return False
        


class Face():

    def __init__(self, _ver, parent):
        self.vertices = np.array(_ver)
        self.parent = parent
        #print(self.parent.vertices[self.vertices[0]].coords())

    def area(self):

        ab = self.parent.vertices[self.vertices[1]].coords() - self.parent.vertices[self.vertices[0]].coords()
        ac = self.parent.vertices[self.vertices[2]].coords() - self.parent.vertices[self.vertices[0]].coords()

        A = 0.5*math.sqrt((ab[1]*ac[2]-ab[2]*ac[1])*(ab[1]*ac[2]-ab[2]*ac[1])+(ab[2]*ac[0]-ab[0]*ac[2])*(ab[2]*ac[0]-ab[0]*ac[2])+(ab[0]*ac[1]-ab[1]*ac[0])*(ab[0]*ac[1]-ab[1]*ac[0]))

        return A

    def is_self_intersecting(self):
        #print('is_self_intersecting() not yet implemented. returns false')
        return False

    def triangulate(self):
        triangles = [Face([self.vertices[0],self.vertices[2],self.vertices[3]], self.parent), Face([self.vertices[0],self.vertices[1],self.vertices[2]], self.parent)]
        return np.array(triangles, dtype = Face)

    def signed_volume(self):
        vertex_0 = self.parent.vertices[self.vertices[0]].coords()
        vertex_1 = self.parent.vertices[self.vertices[1]].coords()
        vertex_2 = self.parent.vertices[self.vertices[2]].coords()

        return vertex_0.dot(np.cross(vertex_1, vertex_2)) / 6.0

    def calculate_normal(self):
        vector_tool = Vectors()
        ab = self.parent.vertices[self.vertices[1]].coords() - self.parent.vertices[self.vertices[0]].coords()
        ac = self.parent.vertices[self.vertices[2]].coords() - self.parent.vertices[self.vertices[0]].coords()

        cross_product = np.cross(ab, ac)
        cross_product /= vector_tool.length(cross_product)
        return np.array(cross_product)

    def __len__(self):
        return len(self.vertices)

    def __str__(self):
        s = str(len(self.vertices))
        for vertex in self.vertices:
            s += " {}".format(vertex)

        return s
