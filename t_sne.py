import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

X = np.loadtxt("mesh_X.txt")
labels = [1,1,1,1,1,1,1,1,1,1,1,
            2,2,2,2,2,2,2,2,2,2,2,
            3,3,3,3,3,3,3,3,3,3,3,
            4,4,4,4,4,4,4,4,4,4,4,
            5,5,5,5,5,5,5,5,5,5,5,
            6,6,6,6,6,6,6,6,6,6,6,
            7,7,7,7,7,7,7,7,7,7,7,
            8,8,8,8,8,8,8,8,8,8,8,
            9,9,9,9,9,9,9,9,9,9,9,
            10,10,10,10,10,10,10,10,10,10,10,
            11,11,11,11,11,11,11,11,11,11,11,
            12,12,12,12,12,12,12,12,12,12,12,
            13,13,13,13,13,13,13,13,13,13,13,
            14,14,14,14,14,14,14,14,14,14,14,
            15,15,15,15,15,15,15,15,15,15,15,
            16,16,16,16,16,16,16,16,16,16,16,
            17,17,17,17,17,17,17,17,17,17,17,
            18,18,18,18,18,18,18,18,18,18,18,
            19,19,19,19,19,19,19,19,19,19,19]


model = TSNE(n_components=2)
tsne_data = model.fit_transform(X)

tsne_data = np.vstack((tsne_data.T, labels)).T
tsne_df = pd.DataFrame(data=tsne_data, columns=('Dim1', 'Dim2', 'labels'))


colors = ['red','green','blue','purple','orange','pink','yellow','grey','black','brown','cyan','olive','lightcoral',
          'lime','navy','teal','chocolate','crimson','peru']

tsne_df.plot(kind= 'scatter', x = 'Dim1', y = 'Dim2', c=labels, cmap=matplotlib.colors.ListedColormap(colors))
plt.show()