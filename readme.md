Paolo had problems with the dependencies which I did not encounter, so I added a script to install and uninstall the needed packages globally in case the local packages do not work.

Run Assignment.py to run the application

The application offers 6 options:

- search\n
The application will first prompt for an input mesh from the database. Next, it'll ask for the query size, after which it will return the k most similar similar objects

- view\n
The application will prompt for an input mesh, which it will then view using PyVista

- evaluate\n
Will calculate the first tier recall for all categories, as well as average of these values

- roc\n
Will prompt for a category, after which it proceeds to plot the roc curve for that category. NB: It is a bit different than a regular roc curve: It computes the specificity and sensitivity for each query and plots the results as discrete points instead of a continuous line. The auc is computed by integrating over all points

- debug\n
puts python in interactive mode for debugging

- quit\n
well..