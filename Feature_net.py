import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv1D, MaxPooling1D, UpSampling1D
from keras import backend as K
import csv
import numpy as np
import code



batch_size = 83
epochs = 12
datafile = open("vertex_data.csv", mode = "r")
datareader = csv.reader(datafile, delimiter=',', quotechar='"')
data = np.zeros((209,1,120000))


for i, mesh in enumerate(datareader):
    for j, val in enumerate(mesh):
        data[i][0][j] = val

print(data.shape)
split = int(len(data) * .8) - 1
train_data = data[0:split]
test_data = data[split:]
input_shape = (1, 120000)

model = Sequential()
model.add(Conv1D(16, kernel_size=4,
    activation='relu',
    input_shape=input_shape))
model.add(Conv1D(32, kernel_size=4,
    activation='relu'))
model.add(MaxPooling1D(pool_size=4))
model.add(Conv1D(64, kernel_size=4,
    activation='relu'))
model.add(Conv1D(128, kernel_size=4,
    activation='relu'))
model.add(MaxPooling1D(pool_size=4))
model.add(Conv1D(128, kernel_size=4,
    activation='relu'))
model.add(Conv1D(256, kernel_size=4,
    activation='relu'))
model.add(MaxPooling1D(pool_size=4))

model.add(UpSampling1D(size = 4))
model.add(Conv1D(256, kernel_size=4,
    activation='relu'))
model.add(Conv1D(128, kernel_size=4,
    activation='relu'))
model.add(UpSampling1D(size = 4))
model.add(Conv1D(128, kernel_size=4,
    activation='relu'))
model.add(Conv1D(64, kernel_size=4,
    activation='relu'))
model.add(UpSampling1D(size = 4))
model.add(Conv1D(32, kernel_size=4,
    activation='relu'))
model.add(Conv1D(16, kernel_size=4,
    activation='relu'))
model.add(Dense(units=120000, activation="softmax"))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(train_data, train_data,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(test_data, test_data))

code.interact(local=locals())