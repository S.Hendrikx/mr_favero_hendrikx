from Mesh import *
from Session import *
import os
import numpy as np
import code
import time

start_time = time.time()

session = Session()
session.load_database("./LabeledDB_new")
session.view_database()

session.import_data_from_csv('processed_db.csv')
session.normalize_data()
session.build_search_tree()

while True:
    mode = input('\nWhat do you want to do? [search, evaluate, roc, debug, quit]\n')
    if mode == 'search':
        id = int(input('Which mesh do you want to use as input? Provide the number before the .ply suffix\n'))
        n = int(input('How many meshes do you want to display?\n'))
        print("\n")
        results = session.search_by_id(id, n)
        print(f"meshes: {results}")

    elif mode == 'debug':
        code.interact(local=locals())
    elif mode == 'quit':
        quit()
    elif mode == 'roc':
        # session.select_features(low = 50)
        cat = input('Which category?\n')
        print(session.roc_curve(cat))
    elif mode == 'evaluate':
        print("\n")
        print("\noverall: ", session.evaluate())
    elif mode == 'view':
        id = int(input('which model would you like to view?\n'))
        session.view_result(id)
    # elif mode == 'tsne':
    #     session.tsne()
pass


# 
# mesh_0 = Mesh()
# mesh_0.import_mesh("./LabeledDB_reduced/Teddy/162.off", "teddy")
# mesh_0.print_metadata()
# freq_0, bins_0 = mesh_0.metadata['angles_3_vertices']
# mesh_1 = Mesh()
# mesh_1.import_mesh("./LabeledDB_reduced/Teddy/169.ply", "teddy")
# mesh_1.print_metadata()
# freq_1, bins_1 = mesh_1.metadata['angles_3_vertices']
# mesh_2 = Mesh()
# mesh_2.import_mesh("./LabeledDB_reduced/Plier/201.off", "Plier")
# mesh_2.print_metadata()
# freq_2, bins_2 = mesh_2.metadata['angles_3_vertices']
#print(session.em_distance(freq_0,freq_0))
#code.interact(local=locals())
#session.process_db_to_csv()
# category = input("Which category would you like to view?\n")
# file = input("Which file would you like to view?\n")
# # category = "Monkey"
# # file = "monkey.ply"
# category = "Teddy"
# file = "162.off"
#session.view_model(category, file)

