import numpy as np
import vg, math
import Vectors

class Features_extraction():

    def init(self):
        pass

    def angle_3_vertices(self, vertex, vertexB, vertexC):         #A3: angle between 3 random vertices
        v1 = np.subtract(vertexB, vertex)
        v2 = np.subtract(vertexC, vertex)
        #v1_norm = vg.normalize(v1)
        #v2_norm = vg.normalize(v2)
        dot_product = np.dot(v1, v2)
        angle3vertices = np.arccos(dot_product)
        #print('angle 3 vertices: ' + str(angle3vertices))
        return angle3vertices

    def dist_to_barycenter(self, vertex):             #D1: distance between barycentre and random vertex
        #vertex_norm = vg.normalize(vertex)
        d2bary = math.sqrt(math.pow(vertex[0], 2) + math.pow(vertex[1], 2) + math.pow(vertex[2], 2))
        #print('distance to barycenter: ' + str(d2bary))
        return d2bary

    def dist_2_vertices(self, vertex, vertexB):                   #D2: distance between 2 random vertices
        dist2vertices = np.subtract(vertexB, vertex)
        #dist2vertices_norm = vg.normalize(dist2vertices)
        d2vert = math.sqrt(math.pow(dist2vertices[0], 2) + math.pow(dist2vertices[1], 2) + math.pow(dist2vertices[2], 2))
        #print('distance 2 vertices: ' + str(d2vert))
        return d2vert

    def sqrt_area_triangle_3_vertices(self, vertex, vertexB, vertexC):
                                                                #D3: square root of area of triangle given by 3 random vertices
        v1 = np.subtract(vertexB, vertex)
        v2 = np.subtract(vertexC, vertex)
        #v1_norm = vg.normalize(v1)
        #v2_norm = vg.normalize(v2)
        area = 0.5 * math.sqrt((v1[1]*v2[2]-v1[2]*v2[1])*(v1[1]*v2[2]-v1[2]*v2[1])
                            +(v1[2]*v2[0]-v1[0]*v2[2])*(v1[2]*v2[0]-v1[0]*v2[2])
                            +(v1[0]*v2[1]-v1[1]*v2[0])*(v1[0]*v2[1]-v1[1]*v2[0]))
        sqrt_area = np.sqrt(area)
        #print('sqrt area: ' + str(sqrt_area))
        return sqrt_area

    def cbrt_vol_tetra_4_vertices(self, vertex, vertexB, vertexC, vertexD):
                                                              #D4: cube root of volume of tetrahedron formed by 4 random vertices
        v1 = np.subtract(vertexB, vertex)
        v2 = np.subtract(vertexC, vertex)
        v3 = np.subtract(vertexD, vertex)
        #v1_norm = vg.normalize(v1)
        #v2_norm = vg.normalize(v2)
        #v3_norm = vg.normalize(v3)
        cross = np.cross(v2, v3)
        print(cross)
        volume = np.dot(v1, cross) / 6
        print(volume)
        cube_root = np.cbrt(volume)
        print('cube root: ' + str(cube_root))
        return cube_root
