from Mesh import *
import pyvista

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class ModelViewer():
    
    def view_with_vista(self, file):
        low_mesh = pyvista.read(file)
        low_mesh.plot(show_edges=True, color='w')


    def __init__(self, mesh = None, category = ""):
        self.category = category
        self.mesh = mesh
        self.mouse_mode = 0
        self.rx = 0
        self.ry = 0
        self.zoom = 1
        self.show_bounding_box = True
        self.show_axes = True
        self.show_principal_components = True
        self.show_test_vector = False
        self.show_model = True
        
    def Init(self):
        glClearColor(.9, .9, .9, 0.0)
        glShadeModel(GL_FLAT)
        glutMotionFunc(self.MotionHandler)
        glutKeyboardFunc(self.KeyboardHandler)

    def Lighting(self): 
        glEnable(GL_LIGHTING)
        BRIGHT4f = (1.0, 1.0, 1.0, 1.0)  # Color for Bright light
        DIM4f = (.7, .7, .7, 1.0)        # Color for Dim light

        glLightfv(GL_LIGHT0, GL_AMBIENT, BRIGHT4f)
        glLightfv(GL_LIGHT0, GL_DIFFUSE, BRIGHT4f)
        glLightfv(GL_LIGHT0, GL_POSITION, (10, 10, 10, 0))
        glEnable(GL_LIGHT0)

        glLightfv(GL_LIGHT1, GL_AMBIENT, DIM4f)
        glLightfv(GL_LIGHT1, GL_DIFFUSE, DIM4f)
        glLightfv(GL_LIGHT1, GL_POSITION, (-10, 10, -10, 0))
        glEnable(GL_LIGHT1)

    def Reshape(self, w, h):
        glViewport(0, 0, w, h) 
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glFrustum(-1.0, 1.0, -1.0, 1.0, 1.5, 20.0)
        glMatrixMode(GL_MODELVIEW)

    def KeyboardHandler(self, key, x, y):
        
        if (key == b'\t'):
            self.mouse_mode = (self.mouse_mode + 1) % 2

    def MotionHandler(self, _x, _y):
        if (self.mouse_mode == 0):
            self.draw(_x, _y)
        else:
            if (_y > 90):
                self.draw(-1, -1, _y/300)
    
    def draw_bounding_box(self):
        
        glBegin(GL_LINES)
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])

        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])

        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])

        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])

        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmin"], self.mesh.bounding_box["ymax"], self.mesh.bounding_box["zmax"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmin"])
        glVertex3f(self.mesh.bounding_box["xmax"], self.mesh.bounding_box["ymin"], self.mesh.bounding_box["zmax"])
        glEnd()

    def draw_axes(self):
        
        glClear(GL_COLOR_BUFFER_BIT)
        glBegin(GL_LINES)
        glColor(1.0, 0.0, 0.0)
        glVertex3f(-5.,0.,0.)
        glVertex3f(5.,0.,0.)
        glColor3f(0.0, 1.0, 0.0)
        glVertex3f(0.,-5.,0.)
        glVertex3f(0.,5.,0.)
        glColor3f(0.0, 0.0, 1.0)
        glVertex3f(0.,0.,-5.)
        glVertex3f(0.,0.,5.)
        glColor3f(0.2, 0.2, 0.2)
        glEnd()

    def draw_principal_components(self, amount=1, scale=1):
        glBegin(GL_LINES)
        principal_components = self.mesh.principal_components()[:amount]
        
        for value, vector in principal_components:
            
            glVertex3f(-vector[0],-vector[1],-vector[2])
            glVertex3f(*vector)
        
        glEnd()

    def draw_test_vector(self):
        glBegin(GL_LINES)
        glVertex(0,0,0)
        glVertex(self.mesh.test_vector[0],self.mesh.test_vector[1],self.mesh.test_vector[2])
        glEnd()
    def draw(self, _rx = 0, _ry = 0, _zoom = 1):

        glClear(GL_COLOR_BUFFER_BIT)
        
        glMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, (.25, .25, .25, 1.0))
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE)
        glEnable(GL_COLOR_MATERIAL)
        glLoadIdentity()

        gluLookAt(0,0,5,0,0,0,0,1,0)
        if (_rx != -1):
            self.rx = _rx
            self.ry = _ry
        else:
            self.zoom = _zoom

        glRotatef(self.rx,0,0,1)
        glRotatef(self.ry,1,0,0)

        glScalef(self.zoom,self.zoom,self.zoom)

        if (self.show_bounding_box):
            self.draw_bounding_box()
        if (self.show_axes):
            self.draw_axes()
        if (self.show_principal_components):
            self.draw_principal_components(2)
        if (self.show_test_vector):
            self.draw_test_vector()

        vertexbuffer = glGenLists(1)

        glNewList(vertexbuffer, GL_COMPILE)
        glBegin(GL_TRIANGLES)
        if (self.show_model):
            for face in self.mesh.faces:
                for index in face.vertices:
                    vertex = self.mesh.vertices[index]
                    if vertex.has_normal():
                        glNormal3f(vertex.nX, vertex.nY, vertex.nZ)
                    glVertex3f(vertex.X, vertex.Y, vertex.Z)

        
        glEnd()
        glEndList()

        glCallList(vertexbuffer)
        #displaylist triangles
        
        glutSwapBuffers()

    def view(self):

        glutInit([])
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE)
        glutInitWindowSize(1000,1000)
        glutCreateWindow(self.category)

        self.Init()
        self.Lighting()
        glutDisplayFunc(self.draw)
        glutReshapeFunc(self.Reshape)

        glutMainLoop()
