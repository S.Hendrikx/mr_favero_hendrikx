import os
from Mesh import *
from ModelViewer import *
import csv
import time
import math
import numpy as np
import random
import matplotlib.pyplot as plt
from scipy.spatial import distance
from scipy.stats import wasserstein_distance
from annoy import AnnoyIndex
from sklearn.manifold import TSNE

class Session():
    
    def __init__(self):
        self.meshes = {}
        self.indexes = {}
        self.processed_meshes = (None,None)
        self.search_tree = None
        self.path = None
        self.debugsjeis = []

    def load_database(self, path):
        self.path = path
        print(self.path)
        for root, dirs, files in os.walk(path):
            
            if root == path:
                for dir in dirs:
                    self.meshes[dir] = []
                    self.indexes[dir] = []
            else:
                print(os.path.split(root)[1])
                print(files)
                self.meshes[os.path.split(root)[1]] = files
                
                self.indexes[os.path.split(root)[1]] = [int(file_.split('.')[0]) for file_ in files if file_.split('.')[1] == "ply"]
            print(self.meshes)
    
    def view_database(self):
        for category in self.indexes:
            print(category)
            print(len(self.indexes[category]))
            for file_ in self.indexes[category]:
                print(f"\t{file_}")
    
    def view_result(self, id):
        viewer = ModelViewer()
        cat = None
        for category in self.indexes:
            if id in self.indexes[category]:
                cat = category

        viewer.view_with_vista("{}/{}/{}.ply".format(self.path, cat,id))

    def view_model(self, file, mode = 0):

        viewer = ModelViewer(file)
        if mode == 0:
            viewer.view_with_vista(file)
        else:
            viewer.view()
        # viewer.view_with_vista("./LabeledDB_reduced/{}/{}".format(category, model))

    def normalize_data(self):
        names, vectors = self.processed_meshes
        vectors = vectors.T

        for i, feature in enumerate(vectors):
            if i < 3:
                feature -= min(feature)
                feature /= max(feature)
                vectors[i] = feature
            else: 
                vectors[i] /= 10

        vectors = vectors.T
        self.processed_meshes = (names, vectors)
        
    def build_search_tree(self):
        
        names, vectors = self.processed_meshes
        annoy_index = AnnoyIndex(len(vectors[0]), "euclidean")
        
        for name, vector in zip(names, vectors):
            id = int(name.split('.')[1].split('/')[len(name.split('.')[1].split('/')) - 1])
            annoy_index.add_item(id, vector)
        
        annoy_index.build(10)
        self.search_tree = annoy_index

    def search_by_vector(self, vector, n = 5):
        return self.search_tree.get_nns_by_vector(vector, n)

    def truncate_number(self, _x, decimals):
        x = _x
        factor = 10 ** decimals
        x *= factor
        x = round(x)
        x /= factor
        return x

    def search_by_id(self, id, n = 5, evaluation = False):
        results = self.search_tree.get_nns_by_item(id, n, include_distances = True)
        
        distances = [self.truncate_number(x, 2) for x in results[1]]
        meshes = results[0]
        classes = []

        for result in meshes:
            for category in self.indexes:
                if result in self.indexes[category]:
                    classes.append(category)
        
        if not evaluation:
            print(f"classes: {classes}")
            print(f"distances: {distances}")
        return meshes

    def resample_db(self):
        for _class in self.meshes:
            for file in self.meshes[_class]:
                if file.split(".")[1] == "off":
                    mesh = Mesh(conv_mode = True)
                    mesh.import_mesh("{}/{}/{}".format(self.path, _class, file), _class)

    def view_results(self, that_array):
        #print or display something
        pass
    
    def process_db_to_csv(self):
        features = ['filename', 'surface_area', 'volume', 'circularity', 'eccentricity', 'angles_3_vertices', 'distances_to_barycenter', 'distances_2_vertices', 'square_roots_area_3_vertices', 'cube_roots_tetrahedron_4_vertices']
        header = ['filename', 'surface_area', 'volume', 'circularity', 'eccentricity', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'angles_3_vertices', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_to_barycenter', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'distances_2_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'square_roots_area_3_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices', 'cube_roots_tetrahedron_4_vertices']
        
        data_file = open(f"{self.path}/processed_db.csv", mode="w+")
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(header)
        
        vertices_file = open("vertex_data.csv", mode="w+")
        vertex_data_writer = csv.writer(vertices_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        
        for _class in self.meshes:
            
            for file in self.meshes[_class]:

                if file.split(".")[1] == "ply":

                    mesh = Mesh()
                    mesh.import_mesh("{}/{}/{}".format(self.path, _class, file), _class)
                    mesh.export_to_ply("{}_normalized/{}/{}".format(self.path, _class, file))
                    
                    vertices = [vertex.coords() for vertex in mesh.vertices]
                    vertices = np.array(vertices, dtype = float).flatten()
                    vertex_data_writer.writerow(vertices)
                    
                    feature_vector = []
                    for feature in features:
                        if type(mesh.metadata[feature]) == tuple:
                            freq, bins = mesh.metadata[feature]
                            feature_vector.extend(list(freq))
                        else:
                            feature_vector.append(mesh.metadata[feature])
                    data_writer.writerow(feature_vector)
            
            
                    
        data_file.close()
        vertices_file.close()
    
    def tsne(self):
        files, features = self.processed_meshes
        tsne = TSNE(n_components=2, random_state=0)
        X_embedded = tsne.fit_transform(features)
        classes = range(0,len(self.meshes))
        plt.figure(figsize=(6, 5))
        colors = ['red','green','blue','purple','orange','pink','yellow','grey','black','brown','cyan','olive','lightcoral',
          'lime','navy','teal','chocolate','crimson','peru']
        target_names = [cat for cat in self.meshes]
        for i, c, label in zip(classes, colors, target_names):
            plt.scatter(X_embedded[y == i, 0], X_embedded[y == i, 1], c=c, label=label)
        plt.legend()
        plt.show()

    def import_data_from_csv(self, csv_file):
        csv_file = open(f"{self.path}/{csv_file}", mode="r")
        reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        data = []
        files = []
        next(reader)
        for line in reader:
            files.append(line[0])
            data.append(np.array(line[1:], dtype=float))
        data = np.array(data, dtype = float)
        files = np.array(files, dtype = str)
        csv_file.close()
        self.processed_meshes = (files, data)

    def euclidean_distance(self, query, target):
        sum_of_squares = 0
        for q, p in zip(query, target):
            sum_of_squares += (q-p)*(q-p)
        return math.sqrt(sum_of_squares)

    def manhattan_distance(self, query, target):
        distance = 0
        for q, p in zip(query, target):
            distance += abs(q-p)
        return distance

    def em_distance(self, query, target):
        return wasserstein_distance(query, target)

    def evaluate(self, samples_per_category = 1):
        val = 0
        for category in self.indexes:
            recall = 0
            for query in self.indexes[category]:
                result = self.search_by_id(query, 10, evaluation=True)[1:]
                result = [1 if x in self.indexes[category] else 0 for x in result]
                #print(f"{category}: {sum(result) / 10}")
                recall += sum(result) / 10
            print(f"{category}: {recall/20}")
            val += recall/20

            # query = random.choice(self.indexes[category])
            # result = self.search_by_id(query, len(self.indexes[category]))[1:]
            # result = [1 if x in self.indexes[category] else 0 for x in result]
            # print(f"{category}: {sum(result) / 10}")
            # val += sum(result) / 10
            
            
        val /= len(self.indexes)
        return val

    def roc_curve(self, query_category):
        
        
        for j, category in enumerate(self.indexes):
            if category == query_category:
                sensitivity = []
                specificity = []
                for i in range(380):
                    for query in self.indexes[category]:
                        query = random.choice(self.indexes[category])
                        result = self.search_by_id(query, i, evaluation=True)[1:]
                        result = [1 if x in self.indexes[category] else 0 for x in result]
                        sensitivity.append(sum(result) / 10)
                        result = [0 if x == 1 else 1 for x in result]
                        specificity.append(1-(sum(result) / 199))
                        
                auc = 0
                for x,y in zip(sensitivity, specificity):
                    auc += x*y
                print(f"auc: {auc}")
                plt.plot(sensitivity, specificity, 'ro')
            

        auc = 0
        for x,y in zip(sensitivity, specificity):
            auc += x*y
        print(f"auc: {auc}")  
        plt.xlabel('sensitivity')
        plt.ylabel('specificity')
        plt.show()
            # query = random.choice(self.indexes[category])
            # result = self.search_by_id(query, len(self.indexes[category]))[1:]
            # result = [1 if x in self.indexes[category] else 0 for x in result]
            # print(f"{category}: {sum(result) / 10}")
            # val += sum(result) / 10
            