import math
import numpy as np
import warnings
class Vectors():
    def __init__(self):
        pass

    def length(self, vector):
        sum = 0
        for val in vector:
            sum += val*val
        return math.sqrt(sum)
    
    def angle(self, vector_1, vector_2, v0 = None,v1 = None,v2 = None):
        warnings.simplefilter("error")
        
        dot_product = np.dot(vector_1, vector_2)
        length_product = self.length(vector_1) * self.length(vector_2)
        try:
            return np.arccos(dot_product / length_product)
        except:
            print(f"v0: {v0}")
            print(f"v1: {v1}")
            print(f"v2: {v2}")
            print(f"dot_product{dot_product}")
            print(f"length_product{length_product}")
            return 0

    